package com.qiniu.ohos.dns;

/**
 */
public interface IpSorter {
    String[] sort(String[] ips);
}
