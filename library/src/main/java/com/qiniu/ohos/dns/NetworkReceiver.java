package com.qiniu.ohos.dns;


import ohos.app.Context;
import ohos.event.commonevent.CommonEventData;
import ohos.event.commonevent.CommonEventSubscribeInfo;
import ohos.event.commonevent.CommonEventSubscriber;
import ohos.net.NetManager;
import ohos.utils.net.Uri;


/**
 */
public final class NetworkReceiver extends CommonEventSubscriber {
    private static final Uri PREFERRED_APN_URI = Uri
            .parse("content://telephony/carriers/preferapn");
    private static DnsManager mdnsManager;
    private static Context mContext;

    public NetworkReceiver(CommonEventSubscribeInfo subscribeInfo, Context context) {
        super(subscribeInfo);
        mContext=context;
    }

    public static NetworkInfo createNetInfo(NetManager info) {
        if (info == null) {
            return NetworkInfo.noNetwork;
        }

        NetworkInfo.NetSatus net=NetworkInfo.NetSatus.WIFI;
        int provider = NetworkInfo.ISP_GENERAL;
//        int main = info.getAppNet().getNetHandle();
//        if (main == ConnectivityManager.TYPE_WIFI) {
//            net = NetworkInfo.NetSatus.WIFI;
//            provider = NetworkInfo.ISP_GENERAL;
//        } else {
//            net = NetworkInfo.NetSatus.MOBILE;
////            参考 http://blog.csdn.net/yinkai1205/article/details/8983861
//
////            判断是否电信:
//
//            final Cursor c = context.getContentResolver().query(
//                    PREFERRED_APN_URI, null, null, null, null);
//            if (c != null) {
//                c.moveToFirst();
//                final String user = c.getString(c
//                        .getColumnIndex("user"));
//                if (!TextUtils.isEmpty(user)) {
//                    if (user.startsWith("ctwap") || user.startsWith("ctnet")) {
//                        provider = NetworkInfo.ISP_CTC;
//                    }
//                }
//            }
//            c.close();
//            if (provider != NetworkInfo.ISP_CTC) {
//// 判断是移动联通wap:
//                String netMode = info.getExtraInfo();
//                if (netMode != null) {
//                    // 通过apn名称判断是否是联通和移动wap
//                    netMode = netMode.toLowerCase(Locale.getDefault());
//
//                    if (netMode.equals("cmwap") || netMode.equals("cmnet")) {
//                        provider = NetworkInfo.ISP_CMCC;
//                    } else if (netMode.equals("3gnet")
//                            || netMode.equals("uninet")
//                            || netMode.equals("3gwap")
//                            || netMode.equals("uniwap")) {
//                        provider = NetworkInfo.ISP_CNC;
//                    }
//                }
//            }
//        }

        return new NetworkInfo(net, provider);
    }

    public static void setDnsManager(DnsManager dnsManager) {
        mdnsManager = dnsManager;
    }

    @Override
    public void onReceiveEvent(CommonEventData commonEventData) {
        if (mdnsManager == null) {
            return;
        }
        NetManager netManager=NetManager.getInstance(mContext);
        NetworkInfo info = createNetInfo(netManager);
        mdnsManager.onNetworkChange(info);
    }
}
