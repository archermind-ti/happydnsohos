# HappyDnsOhos

#### 简介
为OHOS开发的DNS库

#### 功能
方便使用DNS

#### 演示
无


#### 集成

1.  下载模块包关联使用

#### 使用说明

1. 下载模块包源码使用
```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar'])
    implementation project(':library')
    testImplementation 'junit:junit:4.13'
}
```

2. maven引用
```
...
allprojects {
    repositories {
     mavenCentral()
    }
}
...
dependencies {
 ...
 implementation 'com.gitee.archermind-ti:happy_dns_library:1.0.2'
 ...
}
```
#### 使用说明
DnsManager 可以创建一次，一直使用。
```
    IResolver[] resolvers = new IResolver[3];
    resolvers[0] = OhosDnsServer.defaultResolver(getContext()); //系统默认 DNS 服务器
    resolvers[1] = new Resolver(InetAddress.getByName("119.29.29.29")); //自定义 DNS 服务器地址
    resolvers[2] = new QiniuDns(accountId, encryptKey, expireTimeMs); //七牛 http dns 服务
    DnsManager dns = new DnsManager(NetworkInfo.normal(), resolvers);

```
```
其中，七牛 http dns 服务所需的参数如下：

参数	描述
accountId	账户名称，从七牛控制台获取
encryptKey	加密所需的 key，从七牛控制台获取
expireTimeSecond	Unix 时间戳，单位为秒，该时间后请求过期
QiniuDns 提供了 setHttps 与 setEncrypted 两个方法，用于设置是否启用 SSL，与请求的 URL 是否加密。
```
如果软件有国外的使用情况时，建议初始化程序采取这样的方式，下面代码只是根据时区做简单判断，开发者可以根据自己需要使用更精确的判断方式
```
DnsManager dns;
if(DnsManager.needHttpDns()){
	IResolver[] resolvers = new IResolver[2];
    resolvers[0] = new DnspodFree();
    resolvers[1] = OhosDnsServer.defaultResolver(getContext());
    dns = new DnsManager(NetworkInfo.normal, resolvers);
}else{
	IResolver[] resolvers = new IResolver[2];
    resolvers[0] = OhosDnsServer.defaultResolver(getContext());
    resolvers[1] = new Resolver(InetAddress.getByName("8.8.8.8"));
    dns = new DnsManager(NetworkInfo.normal, resolvers);
}



```

#### 编译说明
1. 将项目通过git clone 至本地
2. 使用DevEco Studio 打开该项目，然后等待Gradle 构建完成
3. 点击Run运行即可（真机运行可能需要配置签名）

#### 版本迭代

- v1.0.0   初始版本
- v1.0.1   修改了包名问题
- v1.0.2   修改了文件名问题
- [changelog](https://gitee.com/archermind-ti/happydnsohos/raw/master/changelog.md)

#### 版权和许可信息
### License
The MIT License (MIT)[LICENSE](https://gitee.com/archermind-ti/happydnsohos/raw/master/LICENSE)


