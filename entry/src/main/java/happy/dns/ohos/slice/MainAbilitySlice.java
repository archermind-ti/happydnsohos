package happy.dns.ohos.slice;

import com.qiniu.ohos.dns.DnsManager;
import com.qiniu.ohos.dns.IResolver;
import com.qiniu.ohos.dns.NetworkInfo;
import com.qiniu.ohos.dns.http.QiniuDns;
import com.qiniu.ohos.dns.local.OhosDnsServer;
import com.qiniu.ohos.dns.local.Resolver;
import happy.dns.ohos.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        IResolver[] resolvers = new IResolver[3];
        resolvers[0] = OhosDnsServer.defaultResolver(getContext()); //系统默认 DNS 服务器
        try {
            resolvers[1] = new Resolver(InetAddress.getByName("119.29.29.29")); //自定义 DNS 服务器地址
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        int expireTimeMs=10000;
        resolvers[2] = new QiniuDns("accountId", "encryptKey", expireTimeMs); //七牛 http dns 服务
        //DnsManager dns = new DnsManager(NetworkInfo.normal, resolvers);//使用dns
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
