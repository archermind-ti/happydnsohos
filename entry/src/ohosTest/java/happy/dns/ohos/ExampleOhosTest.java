package happy.dns.ohos;

import com.qiniu.ohos.dns.util.DES;
import com.qiniu.ohos.dns.util.Hex;
import com.qiniu.ohos.dns.util.MD5;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExampleOhosTest {
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("happy.dns.ohos", actualBundleName);
    }
    @Test
    public void TestDes() {
        Assert.assertEquals(DES.encrypt("happy.dns.ohos","123456"), DES.encrypt("happy.dns.ohos","123456"));
    }
    @Test
    public void TestHex() {
        String sss=Hex.encodeHexString("happy.dns.ohos".getBytes());
        Assert.assertNotNull(sss);
        Assert.assertEquals(Hex.encodeHexString("happy.dns.ohos".getBytes()),Hex.encodeHexString("happy.dns.ohos".getBytes()));
    }
    @Test
    public void TestMD5() {
        String sss=MD5.encrypt("happy.dns.ohos");
        Assert.assertNotNull(sss);
        Assert.assertEquals(MD5.encrypt("happy.dns.ohos"),MD5.encrypt("happy.dns.ohos"));
    }
}